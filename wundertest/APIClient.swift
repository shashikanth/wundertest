//
//  APIClient.swift
//  wundertest
//
//  Created by Shashikanth on 12/09/18.
//  Copyright © 2018 Shashikanth. All rights reserved.
//

import Foundation
import Alamofire

enum APIError: String, Error {
    case noNetwork = "No Network"
    case serverOverload = "Server is overloaded"
    case permissionDenied = "You don't have permission"
}

protocol APIClientProtocol {
    func getCarFeed( complete: @escaping ( _ success: Bool, _ photos: [CarModel], _ error: APIError? )->() )
}

class APIClient: APIClientProtocol {
    
    func getCarFeed(complete: @escaping ( _ success: Bool, _ cars: [CarModel], _ error: APIError? ) -> Void) -> () {

        let URL = String(format:"%@", K.URL)
        Alamofire.request(URL)
            .responseJSON  { response in
                let json = response.data
                do{
                    let decoder = JSONDecoder()
                    let apiResponse : APIResponse = try decoder.decode(APIResponse.self, from: json!)
                    let cars : [CarModel] = apiResponse.cars
                    complete(response.result.isSuccess,cars,response.error as? APIError)
                    
                   
                }catch let err{
                    print(err)
                    complete(response.result.isSuccess,[],err as? APIError)
                }
            }
        }
        
        
}
    

