//
//  ViewController+CLLocationManager.swift
//  wundertest
//
//  Created by Shashikanth on 14/09/18.
//  Copyright © 2018 Shashikanth. All rights reserved.
//

import UIKit
import GoogleMaps

extension ViewController: CLLocationManagerDelegate {
    
    func setupLocationManager() {
        locationManager = CLLocationManager()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.distanceFilter = 50
        locationManager.startUpdatingLocation()
        locationManager.delegate = self
        
    }
    
    // Handle incoming location events.
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location: CLLocation = locations.last!
        print("Location: \(location)")
        
        let camera = GMSCameraPosition.camera(withLatitude: location.coordinate.latitude,
                                              longitude: location.coordinate.longitude,
                                              zoom: zoomLevel)
        
        if mapView.isHidden {
            mapView.isHidden = false
            mapView.camera = camera
        } else {
            mapView.animate(to: camera)
        }
        
    }
    
    // Handle authorization for the location manager.
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .restricted:
            showAlert(K.ErrorMessages.locationRestricted)
        case .denied:
            showAlert(K.ErrorMessages.locationAccessDenied)
            mapView.isHidden = false
        case .notDetermined:
            showAlert(K.ErrorMessages.locationNotDetermined)
        case .authorizedAlways: fallthrough
        case .authorizedWhenInUse:
            print(K.ErrorMessages.locationAvailable)
        }
    }
    
    // Handle location manager errors.
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        locationManager.stopUpdatingLocation()
        print("Error: \(error)")
    }
}

