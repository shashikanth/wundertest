//
//  CarTableViewCell.swift
//  wundertest
//
//  Created by Shashikanth on 12/09/18.
//  Copyright © 2018 Shashikanth. All rights reserved.
//

import UIKit

class CarTableViewCell: UITableViewCell {
    
    
    var postFrame: UIView = {
        let frameView = UIView(frame: CGRect(x: 5, y: 5, width: ScreenSize.SCREEN_WIDTH-10, height: 80))
        frameView.backgroundColor = .white
        frameView.layer.shadowColor = UIColor.black.cgColor
        frameView.layer.shadowOpacity = 0.5
        frameView.layer.shadowOffset = CGSize(width: -0.5, height: 1)
        frameView.layer.shadowRadius = 1
        
        frameView.layer.borderColor = UIColor.lightGray.cgColor
        frameView.layer.borderWidth = 0.5
        
        
        frameView.layer.shadowPath = UIBezierPath(rect: frameView.bounds).cgPath
        frameView.layer.shouldRasterize = true
        frameView.layer.rasterizationScale = UIScreen.main.scale
        
        return frameView
    }()
    
    
    var carNameLabel: UILabel = {
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH-40, height: 40))
        label.numberOfLines = 0
        label.font = UIFont.boldSystemFont(ofSize: 16)
        label.textColor = .black
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    var conditionLabel: UILabel = {
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH-40, height: 25))
        label.font = UIFont.boldSystemFont(ofSize: 12)
        label.backgroundColor = .white
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    
    
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.contentView.addSubview(postFrame)
        postFrame.addSubview(carNameLabel)
        postFrame.addSubview(conditionLabel)
        
        postFrame.layer.cornerRadius = 15
        postFrame.layer.masksToBounds = true
        
        carNameLabel.preferredMaxLayoutWidth = ScreenSize.SCREEN_WIDTH-40
        self.selectionStyle = .none
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        carNameLabel.topAnchor.constraint(equalTo: postFrame.topAnchor, constant: 10).isActive = true
        carNameLabel.leftAnchor.constraint(equalTo: postFrame.leftAnchor, constant: 10).isActive = true
        
        conditionLabel.topAnchor.constraint(equalTo: carNameLabel.bottomAnchor, constant: 10).isActive = true
        conditionLabel.leftAnchor.constraint(equalTo: postFrame.leftAnchor, constant: 10).isActive = true
        conditionLabel.heightAnchor.constraint(equalToConstant: 25).isActive = true

        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func confiqureUserCell(item: CarCellViewModel){
        self.carNameLabel.text = item.nameText
        self.conditionLabel.text = item.conditionText
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
}
