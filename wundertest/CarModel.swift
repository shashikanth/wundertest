//
//  CarModel.swift
//  wundertest
//
//  Created by Shashikanth on 12/09/18.
//  Copyright © 2018 Shashikanth. All rights reserved.
//

import UIKit

struct APIResponse: Codable {
    let cars: [CarModel]
    private enum CodingKeys: String, CodingKey {
        case cars = "placemarks"
        
    }
}

struct CarModel: Codable {

    var address: String?
    var coordinates : [Double]?
    var engineType : String?
    var exterior : String?
    var fuel: Int?
    var interior: String?
    var name: String
    var vin: String?
   
    
}
