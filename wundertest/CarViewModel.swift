//
//  CarViewModel.swift
//  wundertest
//
//  Created by Shashikanth on 12/09/18.
//  Copyright © 2018 Shashikanth. All rights reserved.
//

import UIKit


class CarViewModel: NSObject {
    
    var apiClient: APIClientProtocol
    
    private var cars: [CarModel] = [CarModel]()
    
    private var cellViewModels: [CarCellViewModel] = [CarCellViewModel]() {
        didSet {
            self.reloadTableViewClosure?()
        }
    }
    
    var isLoading: Bool = false {
        didSet {
            self.updateLoadingStatus?()
        }
    }
    
    var alertMessage: String? {
        didSet {
            self.showAlertClosure?()
        }
    }
    
    var numberOfCells: Int {
        return (self.selectedCar != nil) ? 1 :cellViewModels.count
    }
    
    
    var selectedCar: CarModel? {
        didSet {
            self.reloadTableViewClosure?()
        }
    }

    
    var reloadTableViewClosure: (()->())?
    var showAlertClosure: (()->())?
    var updateLoadingStatus: (()->())?
    
    
    init( apiClient: APIClientProtocol = APIClient()) {
        self.apiClient = apiClient
    }
    
    func initFetch() {
        self.isLoading = true
        apiClient.getCarFeed { [weak self] (success, cars, error) in
            self?.isLoading = false
            if let error = error {
                self?.alertMessage = error.rawValue
            } else {
                self?.processFetchedCars(cars: cars)
            }

        }
    }

    func getCellViewModel( at indexPath: IndexPath ) -> CarCellViewModel {
        return (self.selectedCar != nil) ? createCellViewModel(car: self.selectedCar!) : cellViewModels[indexPath.row]
    }
    
    
    func createCellViewModel( car: CarModel ) -> CarCellViewModel {
        
        var descTextContainer: [String] = [String]()
        if let exterior = car.exterior {
            descTextContainer.append(String(format: "Exterior : %@",exterior))
        }
        if let interior = car.interior {
            descTextContainer.append( String(format: "Interior : %@",interior) )
        }
        let desc = descTextContainer.joined(separator: " | ")
        
        let latitude : Double = car.coordinates?[1] ?? 0.0
        let longitude : Double = car.coordinates?[0] ?? 0.0
        
        return CarCellViewModel( nameText: car.name,
                                 conditionText: desc,latitude : latitude,longitude: longitude)
    }
    
    
    
    private func processFetchedCars( cars: [CarModel] ) {
        self.cars = cars
        UserDefaults.standard.set(try? PropertyListEncoder().encode(cars), forKey: K.placemarks)
        var vms = [CarCellViewModel]()
        for car in cars {
            vms.append( createCellViewModel(car: car) )
        }
        self.cellViewModels = vms
    }
    
    func userPressed( at indexPath: IndexPath ){
        let car = self.cars[indexPath.row]
        if self.selectedCar != nil {
            self.selectedCar = nil
        }else {
            self.selectedCar = car
        }
    }
  
}

struct CarCellViewModel {
    let nameText: String
    let conditionText: String
    let latitude: Double
    let longitude: Double
}

