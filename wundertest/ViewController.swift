//
//  ViewController.swift
//  wundertest
//
//  Created by Shashikanth on 12/09/18.
//  Copyright © 2018 Shashikanth. All rights reserved.
//

import UIKit
import GoogleMaps
import NVActivityIndicatorView

class ViewController: UIViewController {

    var locationManager = CLLocationManager()
    var currentLocation: CLLocation?
    var mapView: GMSMapView!
    var zoomLevel: Float = 15.0

    var carFeedTableView: UITableView!
    var carViewModel : CarViewModel = CarViewModel()
    var refreshControl: UIRefreshControl!
    var activityIndicator: NVActivityIndicatorView!
    

    func setupTableView() {
        let (_,remainder) = view.frame.divided(atDistance: ScreenSize.SCREEN_HEIGHT/2, from: .minYEdge)
        carFeedTableView = UITableView(frame: remainder, style: .plain)
        carFeedTableView.delegate = self
        carFeedTableView.dataSource = self
        view.addSubview(carFeedTableView)
        carFeedTableView.separatorStyle = .none
        
        carFeedTableView.register(CarTableViewCell.self, forCellReuseIdentifier: K.Cells.cars)
        
        addrefresh()
        getData()

    }
    
    func addrefresh(){
        refreshControl = UIRefreshControl()
        refreshControl.tintColor = .darkGray
        refreshControl.addTarget(self, action: #selector(ViewController.getData), for: .valueChanged)
        carFeedTableView.addSubview(refreshControl)
        
    }
    
    @objc func getData(){
        self.carViewModel.initFetch()
    }
    
    func reloadMarkers() {
        mapView.clear()
       
        for idx in 0...carViewModel.numberOfCells-1 {
            
            let cellVM = carViewModel.getCellViewModel( at: IndexPath(row: idx, section: 0) )

            let marker = GMSMarker()
            marker.position = CLLocationCoordinate2D(latitude:cellVM.latitude, longitude: cellVM.longitude)
            marker.title = cellVM.nameText
            marker.map = mapView
            
            marker.userData = [K.carIndex : idx]
            
            if carViewModel.selectedCar != nil {
                mapView.selectedMarker = marker
            }
            
        }
        
        if carViewModel.numberOfCells > 0 {
            let cellVM = carViewModel.getCellViewModel( at: IndexPath(row: carViewModel.numberOfCells-1, section: 0) )
            mapView.animate(toLocation: CLLocationCoordinate2D(latitude: cellVM.latitude, longitude: cellVM.longitude))
        }
    }
    
    func setupMapView() {
        let (slice,_) = view.frame.divided(atDistance: ScreenSize.SCREEN_HEIGHT/2, from: .minYEdge)
        let camera = GMSCameraPosition.camera(withLatitude: -33.86, longitude: 151.20, zoom: 6.0)
        mapView = GMSMapView.map(withFrame: slice, camera: camera)
        mapView.delegate = self
        mapView.isMyLocationEnabled = true
        view.addSubview(mapView)
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()

        initView()
        initVM()
       
    }

    func initView() {
        setupLocationManager()
        setupMapView()
        setupTableView()
        activityIndicator = NVActivityIndicatorView(frame: view.bounds, type: .ballClipRotateMultiple, color: .blue , padding : 100)
        view.addSubview(activityIndicator)
    }
    
    
    func initVM() {
        
        carViewModel.showAlertClosure = { [weak self] () in
            DispatchQueue.main.async {
                if let message = self?.carViewModel.alertMessage {
                    self?.showAlert( message )
                }
            }
        }
        
        carViewModel.updateLoadingStatus = { [weak self] () in
            DispatchQueue.main.async {
                let isLoading = self?.carViewModel.isLoading ?? false
                if isLoading {
                    self?.activityIndicator.startAnimating()
                }else {
                    self?.activityIndicator.stopAnimating()
                }
            }
        }
        
        carViewModel.reloadTableViewClosure = { [weak self] () in
            DispatchQueue.main.async {
                self?.refreshControl.endRefreshing()
                self?.carFeedTableView.reloadData()
                self?.reloadMarkers()
            }
        }
        
        carViewModel.initFetch()
    }
    
    func showAlert( _ message: String ) {
        let alert = UIAlertController(title: "Alert", message: message, preferredStyle: .alert)
        alert.addAction( UIAlertAction(title: "Ok", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}



extension ViewController: UITableViewDataSource, UITableViewDelegate {
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return K.Titles.cars
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return carViewModel.numberOfCells
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let carCell = tableView.dequeueReusableCell(withIdentifier: K.Cells.cars, for: indexPath) as! CarTableViewCell
        let cellVM = carViewModel.getCellViewModel( at: indexPath )
        carCell.confiqureUserCell(item: cellVM)

        return carCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.carViewModel.userPressed(at: indexPath)
    }
}

extension ViewController : GMSMapViewDelegate {
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        let userInfo : [String : Int]  = marker.userData as! [String : Int]
        self.carViewModel.userPressed(at: IndexPath.init(row: userInfo[K.carIndex]!, section: 0) )
        return false
    }
}

