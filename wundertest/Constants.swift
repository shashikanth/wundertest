//
//  Constants.swift
//  wundertest
//
//  Created by Shashikanth on 12/09/18.
//  Copyright © 2018 Shashikanth. All rights reserved.
//

import UIKit

struct K {
  
    static let URL = "https://s3-us-west-2.amazonaws.com/wunderbucket/locations.json"
    static let googleMapsAPIKey = "AIzaSyCzpN-u0GsE9YpQd6bu92J3lwI2KWq0NR8"
    static let carIndex = "carIndex"
    static let placemarks = "placemarks"

    struct Cells {
        static let cars = "CarsCollectionViewCell"
    }
    
    struct Titles {
        static let cars = "Car Feed"
    }
  
    
    struct ErrorMessages {
        static let locationRestricted = "Location access was restricted."
        static let locationAccessDenied = "User denied access to location."
        static let locationNotDetermined = "Location status not determined."
        static let locationAvailable = "Location status is OK."
    }
    
    
    
 
}

typealias DownloadComplete = () -> ()

struct ScreenSize {
    static let SCREEN_WIDTH         = UIScreen.main.bounds.size.width
    static let SCREEN_HEIGHT        = UIScreen.main.bounds.size.height
    static let SCREEN_MAX_LENGTH    = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
    static let SCREEN_MIN_LENGTH    = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
}

