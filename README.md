
Installation: 

- clone the repo
- run `pod install`
- open wundertest.xcworkspace

Features:
- Fetch Car Feed from URL
- persist them in userdefaults
- show them in list and map views
- on tapping, car name shown on map 
- on tapping again, all cars are shown
