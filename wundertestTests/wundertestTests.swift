//
//  wundertestTests.swift
//  wundertestTests
//
//  Created by Shashikanth on 12/09/18.
//  Copyright © 2018 Shashikanth. All rights reserved.
//

import XCTest
@testable import wundertest

class wundertestTests: XCTestCase {
    
    var sut: CarViewModel!
    var mockAPIClient: MockAPIClient!

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        mockAPIClient = MockAPIClient()
        sut = CarViewModel(apiClient: mockAPIClient)
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        sut = nil
        mockAPIClient = nil
        super.tearDown()
    }
    
    func test_fetch_cars() {
        // Given
        mockAPIClient.cars = [CarModel]()
        
        // When
        sut.initFetch()
        
        // Assert
        XCTAssert(mockAPIClient!.isCarFeedCalled)
    }
    
    
    func test_fetch_cars_fail() {
        
        // Given a failed fetch with a certain failure
        let error =   APIError.permissionDenied
        
        // When
        sut.initFetch()
        
        mockAPIClient.fetchFail(error: error )
        
        // Sut should display predefined error message
        XCTAssertEqual( sut.alertMessage, error.rawValue )
        
    }
    
    
    func test_create_cell_view_model() {
        // Given
        let cars = StubGenerator().stubCars()
        mockAPIClient.cars = cars
        let expect = XCTestExpectation(description: "reload closure triggered")
        sut.reloadTableViewClosure = { () in
            expect.fulfill()
        }
        
        // When
        sut.initFetch()
        mockAPIClient.fetchSuccess()
        
        // Number of cell view model is equal to the number of cars
        XCTAssertEqual( sut.numberOfCells, cars.count )
        
        // XCTAssert reload closure triggered
        wait(for: [expect], timeout: 1.0)
        
    }

    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    
    func test_loading_when_fetching() {
        
        //Given
        var loadingStatus = false
        let expect = XCTestExpectation(description: "Loading status updated")
        sut.updateLoadingStatus = { [weak sut] in
            loadingStatus = sut!.isLoading
            expect.fulfill()
        }
        
        //when fetching
        sut.initFetch()
        
        // Assert
        XCTAssertTrue( loadingStatus )
        
        // When finished fetching
        mockAPIClient!.fetchSuccess()
        XCTAssertFalse( loadingStatus )
        
        wait(for: [expect], timeout: 1.0)
    }
    
    func test_user_press_car() {
        
        //Given a sut with fetched cars
        let indexPath = IndexPath(row: 0, section: 0)
        goToFetchCarFinished()
        
        //When
        sut.userPressed( at: indexPath )
        
        //Assert
        XCTAssertNotNil( sut.selectedCar )
        let expect = XCTestExpectation(description: "reload closure triggered")
        sut.reloadTableViewClosure = { () in
            expect.fulfill()
        }
        
    }
    
    func test_user_press_selected_car() {
        
        //Given a sut with fetched cars
        let indexPath = IndexPath(row: 0, section: 0)
        goToFetchCarFinished()
       
        //When
        sut.userPressed( at: indexPath )
        sut.userPressed( at: indexPath )

        //Assert
        XCTAssertNil( sut.selectedCar )
        let expect = XCTestExpectation(description: "reload closure triggered")
        sut.reloadTableViewClosure = { () in
            expect.fulfill()
        }
        
    }
    
    func test_get_cell_view_model() {
        
        //Given a sut with fetched cars
        goToFetchCarFinished()
        
        let indexPath = IndexPath(row: 1, section: 0)
        let testCar = mockAPIClient.cars[indexPath.row]
        
        // When
        let vm = sut.getCellViewModel(at: indexPath)
        
        //Assert
        XCTAssertEqual( vm.nameText, testCar.name)
        
    }
    
    func test_cell_view_model() {
        //Given cars
        goToFetchCarFinished()

        let indexPath = IndexPath(row: 1, section: 0)
        let car = mockAPIClient.cars[indexPath.row]
        
        // When create cell view model
        let cellViewModel = sut!.createCellViewModel( car: car )
        
        // Assert
        XCTAssertEqual( car.name, cellViewModel.nameText )
        XCTAssertEqual(cellViewModel.conditionText, "Exterior : \(car.exterior!) | Interior : \(car.interior!)" )
        
    }

    
}

extension wundertestTests {
    private func goToFetchCarFinished() {
        mockAPIClient.cars = StubGenerator().stubCars()
        sut.initFetch()
        mockAPIClient.fetchSuccess()
    }
}


class MockAPIClient: APIClientProtocol {
    
    var isCarFeedCalled = false
    
    var cars: [CarModel] = [CarModel]()
    var completeClosure: ((Bool, [CarModel], APIError?) -> ())!
    
    func getCarFeed(complete: @escaping (Bool, [CarModel], APIError?) -> ()) {
        isCarFeedCalled = true
        completeClosure = complete
        
    }
    
    func fetchSuccess() {
        completeClosure( true, cars, nil )
    }
    
    func fetchFail(error: APIError?) {
        completeClosure( false, cars, error )
    }
    
}


class StubGenerator {
    func stubCars() -> [CarModel] {
        let path = Bundle.main.path(forResource: "locations", ofType: "json")!
        let data = try! Data(contentsOf: URL(fileURLWithPath: path))
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .iso8601
        let apiResponse = try! decoder.decode(APIResponse.self, from: data)
        return apiResponse.cars
    }
}
