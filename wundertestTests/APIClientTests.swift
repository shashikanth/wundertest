//
//  APIClientTests.swift
//  wundertestTests
//
//  Created by Shashikanth on 14/09/18.
//  Copyright © 2018 Shashikanth. All rights reserved.
//

import XCTest
@testable import wundertest

class APIClientTests: XCTestCase {
   
    var sut: APIClient?
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        sut = APIClient()

    }
    
    override func tearDown() {
        sut = nil
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func test_fetch_cars() {
        
        // Given An API Client
        let sut = self.sut!
        
        // When fetch all cars
        let expect = XCTestExpectation(description: "callback")
        
        sut.getCarFeed(complete: { (success, cars, error) in
            expect.fulfill()
            XCTAssertEqual( cars.count, 423)
            for car in cars {
                XCTAssertNotNil(car.name)
            }
            
        })
        
        wait(for: [expect], timeout: 3.1)
    }
    
  
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
